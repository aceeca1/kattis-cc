#include <cstdio>
using namespace std;

struct Solution {
    int N;
    
    void Solve() {
        scanf("%d", &N);
        for (int i = 1; i <= N; ++i) printf("%d Abracadabra\n", i);
    }
};

int main() {
    Solution().Solve();
    return 0;
}
